#!/bin/sh

rm dist/*.bin || mkdir -p dist
tmpdir=$(mktemp -d)

wget -qO $tmpdir/master.zip https://github.com/eried/ArduboyCollection/archive/refs/heads/master.zip
unzip -d $tmpdir $tmpdir/master.zip */*/*/*.hex

for base in $tmpdir/*/*/*; do
    if [ -f "$base"/*.hex ]; then
        name=$(basename "$base"|sed -E 's/[ _!-]+/_/')
        for hexname in "$base"/*.hex; do
            hex2bin.py -l 32000 "$hexname" "dist/$name.bin"
        done
    fi
done
rm -rf $tmpdir