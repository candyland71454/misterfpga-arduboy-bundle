# MisterFPGA Arduboy Bundle

Binary version of the awesome [Erwin's Arduboy Collection](https://arduboy.ried.cl/)
suitable for the [MisterFPGA Arduboy core](https://github.com/MiSTer-devel/Arduboy_MiSTer).

Every Wednesday, [Erwin's repository](https://github.com/eried/ArduboyCollection) is automatically checked and, if necessary, a new build zip is generated.

[Grab the latest build zip from here](https://gitlab.com/ergoithz/misterfpga-arduboy-bundle/-/jobs/artifacts/main/download?job=bundle).